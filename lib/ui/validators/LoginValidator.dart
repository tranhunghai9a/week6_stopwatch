mixin LoginValidator {
  String? validateEmail(String? email) {
    if (email!.isEmpty) {
      return 'pls input Email';
    }
    final regex = RegExp('[^@]+@[^\.]+\..+');
    if (!regex.hasMatch(email)){
      return 'pls input valid Email';
    }
    return null;
  }

  String? validatePassword(String? password){
    if(password!.length < 4){
      return 'Password must be at least 4 characters';
    }
  }
}